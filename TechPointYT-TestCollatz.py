#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    def test_read2(self):
        s = "21 30\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  21)
        self.assertEqual(j, 30)
    def test_read3(self):
        s = "31 40\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 31)
        self.assertEqual(j, 40)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(838057, 595611)
        self.assertEqual(v, 525)
        

    def test_eval_2(self):
        v = collatz_eval(630446, 381044)
        self.assertEqual(v, 509)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)
                

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(5000, 8000)
        self.assertEqual(v, 262)

    def test_eval_6(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_7(self):
        v = collatz_eval(648, 1243)
        self.assertEqual(v, 182)
   

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    def test_print2(self):
        w = StringIO()
        collatz_print(w, 100, 110, 120)
        self.assertEqual(w.getvalue(), "100 110 120\n")
    def test_print3(self):
        w = StringIO()
        collatz_print(w, 90000, 90010, 90020)
        self.assertEqual(w.getvalue(), "90000 90010 90020\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    def test_solve2(self):
        r = StringIO("10001 10100\n50000 50100\n100000 100050\n 900000 900090\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10001 10100 224\n50000 50100 234\n100000 100050 310\n900000 900090 370\n")
    def test_solve3(self):
        r = StringIO("6000 6080\n7000 7080\n8000 8080\n9000 9080\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "6000 6080 187\n7000 7080 195\n8000 8080 190\n9000 9080 185\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
